package com.fortress.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fortress.bean.UiDashboard;
import com.fortress.entity.Dashboard;

@Component
public class DashboardConverter extends AbstractConverter<Dashboard, UiDashboard> {

	@Override
	public UiDashboard getBeanFromEntity(Dashboard entity) {

		UiDashboard bean = new UiDashboard();

		bean.setId( entity.getId() );
		bean.setAggressive(entity.getAggressive());
		bean.setConservative(entity.getConservative());
		bean.setAssumedWorkingAge(entity.getAssumedWorkingAge());
		bean.setAvcContributionBelowCeiling(entity.getAvcContributionBelowCeiling());
		bean.setAvcContributionAboveCeiling( entity.getAvcContributionAboveCeiling() );
		bean.setMandatoryRetirementAge(entity.getMandatoryRetirementAge());
		bean.setNisCeiling(entity.getNisCeiling());
		bean.setRegularContributionAboveCeiling(entity.getRegularContributionAboveCeiling());
		bean.setRegularContributionBelowCeiling(entity.getRegularContributionBelowCeiling());

		return bean;
	}

	@Override
	public Dashboard getEntityFromBean(UiDashboard bean) {

		Dashboard entity = new Dashboard();

		populateEntityWithBean(entity, bean);

		return entity;
	}

	@Override
	public List<Dashboard> getEntitiesFromBeans(List<UiDashboard> beans) {

		return null;
	}

	@Override
	public List<UiDashboard> getBeansFromEntities(List<Dashboard> entities) {

		return null;
	}

	@Override
	public void populateEntityWithBean(Dashboard entity, UiDashboard bean) {

		entity.setAggressive(bean.getAggressive());
		entity.setConservative(bean.getConservative());
		entity.setAssumedWorkingAge(bean.getAssumedWorkingAge());
		entity.setAvcContributionBelowCeiling(bean.getAvcContributionBelowCeiling());
		entity.setAvcContributionAboveCeiling( bean.getAvcContributionAboveCeiling() );
		entity.setMandatoryRetirementAge(bean.getMandatoryRetirementAge());
		entity.setNisCeiling(bean.getNisCeiling());
		entity.setRegularContributionAboveCeiling(bean.getRegularContributionAboveCeiling());
		entity.setRegularContributionBelowCeiling(bean.getRegularContributionBelowCeiling());
		
	}

}
