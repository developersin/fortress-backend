package com.fortress.bean;

public class RegularPensionContributions {

	// THESE FOUR FIELDS SET IN THE DATABASE
	private Float employeeBelowNISCeiling;
	private Float employeeAboveNISCeiling;

	private Float employerAVCContributionBelowNISCeiling;
	private Float employerAVCContributionAboveNISCeiling;

	private Float employeeBelowNISCeilingCalculation;
	private Float employeeAboveNISCeilingCalculation;

	private Float employerBelowNISCeilingCalculation;
	private Float employerAboveNISCeilingCalculation;

	private Float totalRegularPensionContributions;

	public Float getEmployeeBelowNISCeiling() {
		return employeeBelowNISCeiling;
	}

	public void setEmployeeBelowNISCeiling(Float employeeBelowNISCeiling) {
		this.employeeBelowNISCeiling = employeeBelowNISCeiling;
	}

	public Float getEmployeeAboveNISCeiling() {
		return employeeAboveNISCeiling;
	}

	public void setEmployeeAboveNISCeiling(Float employeeAboveNISCeiling) {
		this.employeeAboveNISCeiling = employeeAboveNISCeiling;
	}

	public Float getEmployeeBelowNISCeilingCalculation() {
		return employeeBelowNISCeilingCalculation;
	}

	public void setEmployeeBelowNISCeilingCalculation(Float employeeBelowNISCeilingCalculation) {
		this.employeeBelowNISCeilingCalculation = employeeBelowNISCeilingCalculation;
	}

	public Float getEmployeeAboveNISCeilingCalculation() {
		return employeeAboveNISCeilingCalculation;
	}

	public void setEmployeeAboveNISCeilingCalculation(Float employeeAboveNISCeilingCalculation) {
		this.employeeAboveNISCeilingCalculation = employeeAboveNISCeilingCalculation;
	}

	public Float getEmployerBelowNISCeilingCalculation() {
		return employerBelowNISCeilingCalculation;
	}

	public void setEmployerBelowNISCeilingCalculation(Float employerBelowNISCeilingCalculation) {
		this.employerBelowNISCeilingCalculation = employerBelowNISCeilingCalculation;
	}

	public Float getEmployerAboveNISCeilingCalculation() {
		return employerAboveNISCeilingCalculation;
	}

	public void setEmployerAboveNISCeilingCalculation(Float employerAboveNISCeilingCalculation) {
		this.employerAboveNISCeilingCalculation = employerAboveNISCeilingCalculation;
	}

	public Float getTotalRegularPensionContributions() {
		return totalRegularPensionContributions;
	}

	public void setTotalRegularPensionContributions(Float totalRegularPensionContributions) {
		this.totalRegularPensionContributions = totalRegularPensionContributions;
	}

	public Float getEmployerAVCContributionBelowNISCeiling() {
		return employerAVCContributionBelowNISCeiling;
	}

	public void setEmployerAVCContributionBelowNISCeiling(Float employerAVCContributionBelowNISCeiling) {
		this.employerAVCContributionBelowNISCeiling = employerAVCContributionBelowNISCeiling;
	}

	public Float getEmployerAVCContributionAboveNISCeiling() {
		return employerAVCContributionAboveNISCeiling;
	}

	public void setEmployerAVCContributionAboveNISCeiling(Float employerAVCContributionAboveNISCeiling) {
		this.employerAVCContributionAboveNISCeiling = employerAVCContributionAboveNISCeiling;
	}

}
