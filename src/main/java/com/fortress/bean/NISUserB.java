package com.fortress.bean;

import java.util.Date;

public class NISUserB {

	private Date birthdate;
	private Long retirementAge;
	private Long currentSavingsInPension;
	private Float interestRate;
	private Long monthlySavings;

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Long getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Long retirementAge) {
		this.retirementAge = retirementAge;
	}

	public Long getCurrentSavingsInPension() {
		return currentSavingsInPension;
	}

	public void setCurrentSavingsInPension(Long currentSavingsInPension) {
		this.currentSavingsInPension = currentSavingsInPension;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	public Long getMonthlySavings() {
		return monthlySavings;
	}

	public void setMonthlySavings(Long monthlySavings) {
		this.monthlySavings = monthlySavings;
	}

}
