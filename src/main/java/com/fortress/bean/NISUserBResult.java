package com.fortress.bean;

public class NISUserBResult {

	private int totalEstimatedAccumulatedPension;

	public int getTotalEstimatedAccumulatedPension() {
		return totalEstimatedAccumulatedPension;
	}

	public void setTotalEstimatedAccumulatedPension(int totalEstimatedAccumulatedPension) {
		this.totalEstimatedAccumulatedPension = totalEstimatedAccumulatedPension;
	}

}
