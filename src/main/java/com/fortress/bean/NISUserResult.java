package com.fortress.bean;

public class NISUserResult {

	private Float annualNISPension;
	private Float totalPensionPlanValue;
	private Float taxFreeAmountOfRetirement;
	private Float amountOfInnova;
	private Float estimatedInnovaPensionPayoutFirstYear;
	private Float totalEstimatedPensionIncome;
	private Float pensionIncomeAsPercentageOfSalary;
	private Float lumpsum;
	private Float transferOfInnova;

	public Float getAnnualNISPension() {
		return annualNISPension;
	}

	public void setAnnualNISPension(Float annualNISPension) {
		this.annualNISPension = annualNISPension;
	}

	public Float getTotalPensionPlanValue() {
		return totalPensionPlanValue;
	}

	public void setTotalPensionPlanValue(Float totalPensionPlanValue) {
		this.totalPensionPlanValue = totalPensionPlanValue;
	}

	public Float getTaxFreeAmountOfRetirement() {
		return taxFreeAmountOfRetirement;
	}

	public void setTaxFreeAmountOfRetirement(Float taxFreeAmountOfRetirement) {
		this.taxFreeAmountOfRetirement = taxFreeAmountOfRetirement;
	}

	public Float getAmountOfInnova() {
		return amountOfInnova;
	}

	public void setAmountOfInnova(Float amountOfInnova) {
		this.amountOfInnova = amountOfInnova;
	}

	public Float getEstimatedInnovaPensionPayoutFirstYear() {
		return estimatedInnovaPensionPayoutFirstYear;
	}

	public void setEstimatedInnovaPensionPayoutFirstYear(Float estimatedInnovaPensionPayoutFirstYear) {
		this.estimatedInnovaPensionPayoutFirstYear = estimatedInnovaPensionPayoutFirstYear;
	}

	public Float getTotalEstimatedPensionIncome() {
		return totalEstimatedPensionIncome;
	}

	public void setTotalEstimatedPensionIncome(Float totalEstimatedPensionIncome) {
		this.totalEstimatedPensionIncome = totalEstimatedPensionIncome;
	}

	public Float getPensionIncomeAsPercentageOfSalary() {
		return pensionIncomeAsPercentageOfSalary;
	}

	public void setPensionIncomeAsPercentageOfSalary(Float pensionIncomeAsPercentageOfSalary) {
		this.pensionIncomeAsPercentageOfSalary = pensionIncomeAsPercentageOfSalary;
	}

	public Float getLumpsum() {
		return lumpsum;
	}

	public void setLumpsum(Float lumpsum) {
		this.lumpsum = lumpsum;
	}

	public Float getTransferOfInnova() {
		return transferOfInnova;
	}

	public void setTransferOfInnova(Float transferOfInnova) {
		this.transferOfInnova = transferOfInnova;
	}

}
