package com.fortress.bean;

public class Innova {

	private int ageAtJanuaryOne;
	private int ageAtDecemberThirtyOne;
	private Float maximumPercentage;

	public int getAgeAtJanuaryOne() {
		return ageAtJanuaryOne;
	}

	public void setAgeAtJanuaryOne(int ageAtJanuaryOne) {
		this.ageAtJanuaryOne = ageAtJanuaryOne;
	}

	public int getAgeAtDecemberThirtyOne() {
		return ageAtDecemberThirtyOne;
	}

	public void setAgeAtDecemberThirtyOne(int ageAtDecemberThirtyOne) {
		this.ageAtDecemberThirtyOne = ageAtDecemberThirtyOne;
	}

	public Float getMaximumPercentage() {
		return maximumPercentage;
	}

	public void setMaximumPercentage(Float maximumPercentage) {
		this.maximumPercentage = maximumPercentage;
	}

	public Innova(int ageAtJanuaryOne, int ageAtDecemberThirtyOne, Float maximumPercentage) {
		super();
		this.ageAtJanuaryOne = ageAtJanuaryOne;
		this.ageAtDecemberThirtyOne = ageAtDecemberThirtyOne;
		this.maximumPercentage = maximumPercentage;
	}

}
