package com.fortress.bean;

public class NISUserAResult {

	private Float monthlyInvestmentRequiredToAchieveTarget;

	public Float getMonthlyInvestmentRequiredToAchieveTarget() {
		return monthlyInvestmentRequiredToAchieveTarget;
	}

	public void setMonthlyInvestmentRequiredToAchieveTarget(Float monthlyInvestmentRequiredToAchieveTarget) {
		this.monthlyInvestmentRequiredToAchieveTarget = monthlyInvestmentRequiredToAchieveTarget;
	}

}
