package com.fortress.bean;

public class AnnualCalculationBean {

	private int yearsFromToday;
	private Float annualCeilingMultiplier;
	private Float annualSlaryMultiplier;
	private Float annualCeilingMonthly;
	private Float annualCeilingAnnual;
	private Float salaryMonthly;
	private Float salaryAnnual;

	public int getYearsFromToday() {
		return yearsFromToday;
	}

	public void setYearsFromToday(int yearsFromToday) {
		this.yearsFromToday = yearsFromToday;
	}

	public Float getAnnualCeilingMultiplier() {
		return annualCeilingMultiplier;
	}

	public void setAnnualCeilingMultiplier(Float annualCeilingMultiplier) {
		this.annualCeilingMultiplier = annualCeilingMultiplier;
	}

	public Float getAnnualSlaryMultiplier() {
		return annualSlaryMultiplier;
	}

	public void setAnnualSlaryMultiplier(Float annualSlaryMultiplier) {
		this.annualSlaryMultiplier = annualSlaryMultiplier;
	}

	public Float getAnnualCeilingMonthly() {
		return annualCeilingMonthly;
	}

	public void setAnnualCeilingMonthly(Float annualCeilingMonthly) {
		this.annualCeilingMonthly = annualCeilingMonthly;
	}

	public Float getAnnualCeilingAnnual() {
		return annualCeilingAnnual;
	}

	public void setAnnualCeilingAnnual(Float annualCeilingAnnual) {
		this.annualCeilingAnnual = annualCeilingAnnual;
	}

	public Float getSalaryMonthly() {
		return salaryMonthly;
	}

	public void setSalaryMonthly(Float salaryMonthly) {
		this.salaryMonthly = salaryMonthly;
	}

	public Float getSalaryAnnual() {
		return salaryAnnual;
	}

	public void setSalaryAnnual(Float salaryAnnual) {
		this.salaryAnnual = salaryAnnual;
	}

}
