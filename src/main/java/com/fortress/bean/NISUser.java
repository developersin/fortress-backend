package com.fortress.bean;

import java.util.Date;

import com.fortress.util.AppConstants.InvestmentStyle;

public class NISUser {

	private Date birthdate;
	private Long retirementAge;
	private Float monthlySalary;
	private Float employeeAVCContributions;
	private InvestmentStyle investmentStyle;
	private Float recentPensionStatementBalance;
	private Date pensionStatementDate;
//	private Date dateOfWorkStarted;

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Long getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Long retirementAge) {
		this.retirementAge = retirementAge;
	}

	public Float getEmployeeAVCContributions() {
		return employeeAVCContributions;
	}

	public void setEmployeeAVCContributions(Float employeeAVCContributions) {
		this.employeeAVCContributions = employeeAVCContributions;
	}

	public InvestmentStyle getInvestmentStyle() {
		return investmentStyle;
	}

	public void setInvestmentStyle(InvestmentStyle investmentStyle) {
		this.investmentStyle = investmentStyle;
	}

	public Date getPensionStatementDate() {
		return pensionStatementDate;
	}

	public void setPensionStatementDate(Date pensionStatementDate) {
		this.pensionStatementDate = pensionStatementDate;
	}

	public Float getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(Float monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	public Float getRecentPensionStatementBalance() {
		return recentPensionStatementBalance;
	}

	public void setRecentPensionStatementBalance(Float recentPensionStatementBalance) {
		this.recentPensionStatementBalance = recentPensionStatementBalance;
	}

//	public Date getDateOfWorkStarted() {
//		return dateOfWorkStarted;
//	}
//
//	public void setDateOfWorkStarted(Date dateOfWorkStarted) {
//		this.dateOfWorkStarted = dateOfWorkStarted;
//	}

}
