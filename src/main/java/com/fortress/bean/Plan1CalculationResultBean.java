package com.fortress.bean;

public class Plan1CalculationResultBean {

	private Float employeeRegularContributionBelowNISCeiling;
	private Float employeeRegularContributionAboveNISCeiling;

	private Float employerRegularContributionBelowNISCeiling;
	private Float employerRegularContributionAboveNISCeiling;

	private Float employeeVoluntaryContributionBelowNISCeiling;
	private Float employeeVoluntaryContributionAboveNISCeiling;

	private Float employerVoluntaryContributionBelowNISCeiling;
	private Float employerVoluntaryContributionAboveNISCeiling;

	public Float getEmployeeRegularContributionBelowNISCeiling() {
		return employeeRegularContributionBelowNISCeiling;
	}

	public void setEmployeeRegularContributionBelowNISCeiling(Float employeeRegularContributionBelowNISCeiling) {
		this.employeeRegularContributionBelowNISCeiling = employeeRegularContributionBelowNISCeiling;
	}

	public Float getEmployeeRegularContributionAboveNISCeiling() {
		return employeeRegularContributionAboveNISCeiling;
	}

	public void setEmployeeRegularContributionAboveNISCeiling(Float employeeRegularContributionAboveNISCeiling) {
		this.employeeRegularContributionAboveNISCeiling = employeeRegularContributionAboveNISCeiling;
	}

	public Float getEmployerRegularContributionBelowNISCeiling() {
		return employerRegularContributionBelowNISCeiling;
	}

	public void setEmployerRegularContributionBelowNISCeiling(Float employerRegularContributionBelowNISCeiling) {
		this.employerRegularContributionBelowNISCeiling = employerRegularContributionBelowNISCeiling;
	}

	public Float getEmployerRegularContributionAboveNISCeiling() {
		return employerRegularContributionAboveNISCeiling;
	}

	public void setEmployerRegularContributionAboveNISCeiling(Float employerRegularContributionAboveNISCeiling) {
		this.employerRegularContributionAboveNISCeiling = employerRegularContributionAboveNISCeiling;
	}

	public Float getEmployeeVoluntaryContributionBelowNISCeiling() {
		return employeeVoluntaryContributionBelowNISCeiling;
	}

	public void setEmployeeVoluntaryContributionBelowNISCeiling(Float employeeVoluntaryContributionBelowNISCeiling) {
		this.employeeVoluntaryContributionBelowNISCeiling = employeeVoluntaryContributionBelowNISCeiling;
	}

	public Float getEmployeeVoluntaryContributionAboveNISCeiling() {
		return employeeVoluntaryContributionAboveNISCeiling;
	}

	public void setEmployeeVoluntaryContributionAboveNISCeiling(Float employeeVoluntaryContributionAboveNISCeiling) {
		this.employeeVoluntaryContributionAboveNISCeiling = employeeVoluntaryContributionAboveNISCeiling;
	}

	public Float getEmployerVoluntaryContributionBelowNISCeiling() {
		return employerVoluntaryContributionBelowNISCeiling;
	}

	public void setEmployerVoluntaryContributionBelowNISCeiling(Float employerVoluntaryContributionBelowNISCeiling) {
		this.employerVoluntaryContributionBelowNISCeiling = employerVoluntaryContributionBelowNISCeiling;
	}

	public Float getEmployerVoluntaryContributionAboveNISCeiling() {
		return employerVoluntaryContributionAboveNISCeiling;
	}

	public void setEmployerVoluntaryContributionAboveNISCeiling(Float employerVoluntaryContributionAboveNISCeiling) {
		this.employerVoluntaryContributionAboveNISCeiling = employerVoluntaryContributionAboveNISCeiling;
	}

}
