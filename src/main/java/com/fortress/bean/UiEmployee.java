package com.fortress.bean;

public class UiEmployee {

	private Long id;

	private String name;
	private Integer salary;
	private String password;
	private Long dashboardId;
	private UiDashboard dashboard;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getDashboardId() {
		return dashboardId;
	}

	public void setDashboardId(Long dashboardId) {
		this.dashboardId = dashboardId;
	}

	public UiDashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(UiDashboard dashboard) {
		this.dashboard = dashboard;
	}

}
