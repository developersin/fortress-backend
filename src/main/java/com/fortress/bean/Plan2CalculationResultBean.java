package com.fortress.bean;

public class Plan2CalculationResultBean {

	private Float statemenetFutureValue;
	private Float monthlyFutureContribution;

	public Float getStatemenetFutureValue() {
		return statemenetFutureValue;
	}

	public void setStatemenetFutureValue(Float statemenetFutureValue) {
		this.statemenetFutureValue = statemenetFutureValue;
	}

	public Float getMonthlyFutureContribution() {
		return monthlyFutureContribution;
	}

	public void setMonthlyFutureContribution(Float monthlyFutureContribution) {
		this.monthlyFutureContribution = monthlyFutureContribution;
	}

}
