package com.fortress.bean;

public class Plan2CalculationBean {

	private int yearsFromToday;
	private Float salaryMonthly;
	private Float salaryAnnual;
	private Float salaryInExcessOfInsured;
	private Float ceilingMonthly;
	private Float monthlyRegularContributions;
	private Float monthlyAVCContributions;
	private Float totalMonthlyContributions;
	private Float fvMonthlyContributions;
	private Float compounded;
	private Float cumulativeMonthlyContributions;

	public int getYearsFromToday() {
		return yearsFromToday;
	}

	public void setYearsFromToday(int yearsFromToday) {
		this.yearsFromToday = yearsFromToday;
	}

	public Float getSalaryMonthly() {
		return salaryMonthly;
	}

	public void setSalaryMonthly(Float salaryMonthly) {
		this.salaryMonthly = salaryMonthly;
	}

	public Float getSalaryAnnual() {
		return salaryAnnual;
	}

	public void setSalaryAnnual(Float salaryAnnual) {
		this.salaryAnnual = salaryAnnual;
	}

	public Float getSalaryInExcessOfInsured() {
		return salaryInExcessOfInsured;
	}

	public void setSalaryInExcessOfInsured(Float salaryInExcessOfInsured) {
		this.salaryInExcessOfInsured = salaryInExcessOfInsured;
	}

	public Float getCeilingMonthly() {
		return ceilingMonthly;
	}

	public void setCeilingMonthly(Float ceilingMonthly) {
		this.ceilingMonthly = ceilingMonthly;
	}

	public Float getMonthlyRegularContributions() {
		return monthlyRegularContributions;
	}

	public void setMonthlyRegularContributions(Float monthlyRegularContributions) {
		this.monthlyRegularContributions = monthlyRegularContributions;
	}

	public Float getMonthlyAVCContributions() {
		return monthlyAVCContributions;
	}

	public void setMonthlyAVCContributions(Float monthlyAVCContributions) {
		this.monthlyAVCContributions = monthlyAVCContributions;
	}

	public Float getTotalMonthlyContributions() {
		return totalMonthlyContributions;
	}

	public void setTotalMonthlyContributions(Float totalMonthlyContributions) {
		this.totalMonthlyContributions = totalMonthlyContributions;
	}

	public Float getFvMonthlyContributions() {
		return fvMonthlyContributions;
	}

	public void setFvMonthlyContributions(Float fvMonthlyContributions) {
		this.fvMonthlyContributions = fvMonthlyContributions;
	}

	public Float getCompounded() {
		return compounded;
	}

	public void setCompounded(Float compounded) {
		this.compounded = compounded;
	}

	public Float getCumulativeMonthlyContributions() {
		return cumulativeMonthlyContributions;
	}

	public void setCumulativeMonthlyContributions(Float cumulativeMonthlyContributions) {
		this.cumulativeMonthlyContributions = cumulativeMonthlyContributions;
	}

}
