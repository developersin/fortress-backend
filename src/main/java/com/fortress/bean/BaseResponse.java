package com.fortress.bean;

import java.util.Map;

import com.fortress.util.AppConstants.ResultStatus;

public class BaseResponse {

	private Boolean reloadContext = false;
	private Boolean success = false;
	private ResultStatus resultStatus;
	private String message;
	private Map<String, String> errorMap;

	public Map<String, String> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}

	public ResultStatus getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(ResultStatus resultStatus) {

		if (resultStatus != null && resultStatus == ResultStatus.SUCCESS) {
			this.success = true;
		}

		this.resultStatus = resultStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Boolean getReloadContext() {
		return reloadContext;
	}

	public void setReloadContext(Boolean reloadContext) {
		this.reloadContext = reloadContext;
	}
}
