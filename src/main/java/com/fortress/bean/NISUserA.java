package com.fortress.bean;

import java.util.Date;

public class NISUserA {

	private Date birthdate;
	private Long retirementAge;
	private Long currentSavingsInPension;
	private Float interestRate;
	private Long expectedAccumulationByRetirement;

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Long getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Long retirementAge) {
		this.retirementAge = retirementAge;
	}

	public Long getCurrentSavingsInPension() {
		return currentSavingsInPension;
	}

	public void setCurrentSavingsInPension(Long currentSavingsInPension) {
		this.currentSavingsInPension = currentSavingsInPension;
	}

	public Float getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Float interestRate) {
		this.interestRate = interestRate;
	}

	public Long getExpectedAccumulationByRetirement() {
		return expectedAccumulationByRetirement;
	}

	public void setExpectedAccumulationByRetirement(Long expectedAccumulationByRetirement) {
		this.expectedAccumulationByRetirement = expectedAccumulationByRetirement;
	}

}
