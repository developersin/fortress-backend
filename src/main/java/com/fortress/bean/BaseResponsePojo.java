package com.fortress.bean;

import java.util.List;

public class BaseResponsePojo<ID, T> extends BaseResponse {

	private T object;
	private List<T> objects;
	private ID objectId;
	private List<ID> objectIds;

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public ID getObjectId() {
		return objectId;
	}

	public void setObjectId(ID objectId) {
		this.objectId = objectId;
	}

	public List<T> getObjects() {
		return objects;
	}

	public void setObjects(List<T> objects) {
		this.objects = objects;
	}

	public List<ID> getObjectIds() {
		return objectIds;
	}

	public void setObjectIds(List<ID> objectIds) {
		this.objectIds = objectIds;
	}

}
