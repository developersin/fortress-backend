package com.fortress.util;

public class AppConstants {

	
	// AuditHistory constants
	public static final String COMMA = ",";
	public static final String TYPE = "Type:";
	public static final String SPACE = " ";
	public static final String COLON = ":";

	// Validation regex
	public static final String EMAIL_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	public static final String REGEX_PHONE_NUMBER_DEFAULT = "[0-9-\\s]*";

	// miscellaneous constants
	public static final String DOT = ".";

	public static final String NOT_AVAILABLE = "N/A";
	
	// NIS CALCULATOR PARAMTERS
	public static final Float ANNUAL_SALARY_INCREASE = 0.04f;
	public static final Float ANNUAL_CEILING_INCREASE = 0.03f;
	public static final Float CURRENT_MONTHLY_BASE_ANNUAL_CEILING = 4650f;
	public static final Float FIRST_20_YEARS = 0.02f;
	public static final Float REMAINING_YEARS = 0.0125f;
	public static final Float CAP_PERCENTAGE = 0.6f;

	public interface GenericEnum {
		long getId();

		boolean isActiveIndicator();

		String getLabel();
	}

	public enum AttributeType implements GenericEnum {

		TEXT("text", "text"),INTEGER("integer","integer"),REAL("real", "real"),DATE("date", "date"),DATETIME("dateTime", "dateTime"), RADIO("radio", "radio"), CHECKBOX(
				"checkbox","checkboxes"),SELECT("select", "dropdown"),MULTI_SELECT("multiSelect",
						"multiSelectDropdown"),TEXT_AREA("textArea", "textarea"),FILE("file", "file"),IMAGE("image", "image");

		private String fieldName;
		private String uiFieldName;

		private AttributeType(String fieldName, String uiFieldName) {
			this.fieldName = fieldName;
			this.uiFieldName = uiFieldName; // To bridge the field type names
											// from the Create Edit Model Form.
		}

		public String getFieldName() {
			return this.fieldName;
		}

		public String getUiFieldName() {
			return uiFieldName;
		}

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}
	}

	public enum UserStatus implements GenericEnum {

		ACTIVE(), INACTIVE();

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}
	}
	
	public enum InvestmentStyle implements GenericEnum {
		AGGRESSIVE(), CONSERVATIVE();

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}

		@Override
		public String getLabel() {
			return this.toString();
		}
		
	}

	public enum UserRoleType implements GenericEnum {

		SYSTEM_ADMIN(), COMPANY_ADMIN(), COMPANY_USER();

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}
	}

	public enum Status implements GenericEnum {
		ACTIVE(), INACTIVE();

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}
	}

	public enum AllowMultipleInstances implements GenericEnum {

		YES(), NO();

		@Override
		public long getId() {
			return this.ordinal();
		}

		@Override
		public String getLabel() {
			return this.toString();
		}

		@Override
		public boolean isActiveIndicator() {
			return false;
		}
	}
	
	public enum FileType {
		CSV, XLSX,XLS;
	}

	public enum FetchType {
		FULL, PARTIAL, MINIMAL;
	}

	public enum ResultStatus {
		SUCCESS("SUCCESS"), ERROR("Error"), WARNING("Warning"), DENIED("Denied");

		private String displayStatus;

		private ResultStatus(String displayStatus) {
			this.displayStatus = displayStatus;
		}

		public String getDisplayStatus() {
			return displayStatus;
		}

		public int getValue() {
			return this.ordinal();
		}

		public static ResultStatus getByValue(int value) {
			return ResultStatus.values()[value];
		}

		public String getLabel() {
			return this.toString();
		}
	}
}
