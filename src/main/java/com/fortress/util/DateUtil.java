package com.fortress.util;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	public static Date parseDateFromString(String dateInString, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		Date date;
		try {
			date = formatter.parse(dateInString);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String parseDateToFormat(Date date, String dateFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		return formatter.format(date);
	}

	public static Date getDate(int year, int month, int date) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, date);
		return cal.getTime();
	}

	public static String getTodayDate() {
		return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	}

	public static String getTodayDateTimeFileName() {
		return new SimpleDateFormat("yyyyMMdd_HHmmss_").format(new Date());
	}

	public static int getDiffYears(Date first, Date last) {
		Calendar a = getCalendar(first);
		Calendar b = getCalendar(last);
		int diff = b.get(YEAR) - a.get(YEAR);
		if (a.get(MONTH) > b.get(MONTH) || (a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE))) {
			diff--;
		}
		return diff;
	}

	public static Float getDiffInFloat(Date first, Date last) {
		Calendar a = getCalendar(first);
		Calendar b = getCalendar(last);

		return ( (float) (last.getTime() - first.getTime()) / (1000*60*60*24*365.18f));
	}
	
	public static Float getDiffInExcelFrac(Date first, Date last) {
		Calendar a = getCalendar(first);
		Calendar b = getCalendar(last);

		return ( (float) (last.getTime() - first.getTime()) / (1000*60*60*24*365.00f));
	}

	public static Calendar getCalendar(Date date) {
		Calendar cal = Calendar.getInstance(Locale.US);
		cal.setTime(date);
		return cal;
	}

}
