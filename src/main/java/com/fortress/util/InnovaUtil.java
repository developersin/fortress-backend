package com.fortress.util;

import java.util.ArrayList;
import java.util.List;

import com.fortress.bean.Innova;

public class InnovaUtil {

	private static List<Innova> innovaList = new ArrayList<>();

	public static List<Innova> getInnovaList() {
		return innovaList;
	}

	public static void setInnovaList(List<Innova> innovaList) {
		InnovaUtil.innovaList = innovaList;
	}

    static {

    	innovaList.add( new Innova(55, 56, 0.0582f) );
    	innovaList.add( new Innova(56, 57, 0.0588f) );
    	innovaList.add( new Innova(57, 58, 0.0595f) );
    	innovaList.add( new Innova(58, 59, 0.0603f) );
    	innovaList.add( new Innova(59, 60, 0.0611f) );
    	innovaList.add( new Innova(60, 61, 0.0620f) );
    	innovaList.add( new Innova(61, 62, 0.0629f) );
    	innovaList.add( new Innova(62, 63, 0.0639f) );
    	innovaList.add( new Innova(63, 64, 0.0650f) );
    	innovaList.add( new Innova(64, 65, 0.0663f) );
    	innovaList.add( new Innova(65, 66, 0.0676f) );
    	innovaList.add( new Innova(66, 67, 0.0690f) );
    	innovaList.add( new Innova(67, 68, 0.0706f) );
    	innovaList.add( new Innova(68, 69, 0.0724f) );
    	innovaList.add( new Innova(69, 70, 0.0743f) );
    	innovaList.add( new Innova(70, 71, 0.0764f) );
    	innovaList.add( new Innova(71, 72, 0.0788f) );
    	innovaList.add( new Innova(72, 73, 0.0815f) );
    	innovaList.add( new Innova(73, 74, 0.0845f) );
    	innovaList.add( new Innova(74, 75, 0.0879f) );
    	innovaList.add( new Innova(75, 76, 0.0918f) );
    	innovaList.add( new Innova(76, 77, 0.0962f) );
    	innovaList.add( new Innova(77, 78, 0.1014f) );
    	innovaList.add( new Innova(78, 79, 0.1075f) );
    	innovaList.add( new Innova(79, 80, 0.1147f) );
    	innovaList.add( new Innova(80, 81, 0.1233f) );
    	innovaList.add( new Innova(81, 82, 0.1340f) );
    	innovaList.add( new Innova(82, 83, 0.1474f) );
    	innovaList.add( new Innova(83, 84, 0.1646f) );
    	innovaList.add( new Innova(84, 85, 0.1876f) );
    	innovaList.add( new Innova(85, 86, 0.2200f) );
    	innovaList.add( new Innova(86, 87, 0.2686f) );
    	innovaList.add( new Innova(87, 88, 0.3497f) );
    	innovaList.add( new Innova(88, 89, 0.5122f) );
    	innovaList.add( new Innova(89, 90, 1.00f) );
    	innovaList.add( new Innova(90, 91, 1.00f) );
    }
    
    public static Float innovaFinder(Long retirementAge) {
    	
    	for( int i=0; i < innovaList.size(); i++ ) {    		
    		if(retirementAge.equals( (long) innovaList.get(i).getAgeAtDecemberThirtyOne()  )) {
    		  return innovaList.get(i).getMaximumPercentage();
    		}
    	}
    	
    	return 0.0f;
    }
}
