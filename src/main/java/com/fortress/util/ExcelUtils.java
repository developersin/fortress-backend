package com.fortress.util;

public class ExcelUtils {

	static public double pmt(double r, Float monthsRemainingInRetirement, double pv, double fv, int type) {
	    double pmt = -r * (pv * Math.pow(1 + r, monthsRemainingInRetirement) + fv) / ((1 + r*type) * (Math.pow(1 + r, monthsRemainingInRetirement) - 1));
	    return pmt;
	}
	
}
