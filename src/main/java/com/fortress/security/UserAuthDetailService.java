package com.fortress.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.UiEmployee;
import com.fortress.entity.BasePermission;
import com.fortress.entity.Employee;
import com.fortress.service.EmployeeService;

@Service( "userAuthDetailService" )
public class UserAuthDetailService  implements UserDetailsService{

	@Resource
	private EmployeeService employeeService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		AuthenticatedUser authenticatedUser = null;
		
		try{
			
			Employee user = employeeService.getAuthenticUserByUsername(username);
			
			if( user == null)
			{
				throw new  UsernameNotFoundException( "Username does not exists" );
			}else{
				
				UiEmployee uiEmployee = employeeService.get( user.getId() );
				
				authenticatedUser = new AuthenticatedUser(uiEmployee.getName(), user.getPassword(), getPermissions(user));
				
				UsernamePasswordAuthenticationToken authenticationToken = 
						new UsernamePasswordAuthenticationToken( authenticatedUser.getUsername() , authenticatedUser.getPassword(), authenticatedUser.getAuthorities());

				authenticationToken.setDetails( authenticatedUser );
				
				SecurityContextHolder.getContext().setAuthentication( authenticationToken );
				
			}
			
		}catch( Exception e )
		{
			throw new  UsernameNotFoundException( "Username does not exists" );
		}
		
		return authenticatedUser;
	}
	
	private List<SimpleGrantedAuthority> getPermissions(Employee user) throws Exception
	{
		
		Set<BasePermission> userPermissions = user.getBasePermissions();
		
		List<SimpleGrantedAuthority> simplePermissions = new ArrayList<SimpleGrantedAuthority>();
		
		
		if(userPermissions != null)
		{
			for (BasePermission permission : userPermissions) {
				simplePermissions.add(new SimpleGrantedAuthority(permission.getName()));
			}
		}
		
		return simplePermissions;
	}

}
