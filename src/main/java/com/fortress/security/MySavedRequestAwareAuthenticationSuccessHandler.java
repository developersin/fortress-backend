package com.fortress.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fortress.bean.BaseResponsePojo;
import com.fortress.bean.NISUserAResult;
import com.fortress.service.AuthenticationService;

public class MySavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	private RequestCache requestCache = new HttpSessionRequestCache();

	@Resource
	private AuthenticationService authenticationService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {

		if ("application/json".equals(request.getHeader("Content-Type"))) {

			ObjectMapper objectMapper = new ObjectMapper();

			BaseResponsePojo<Long, AuthenticatedUser> REST_RESPONSE = new BaseResponsePojo<Long, AuthenticatedUser>();
			REST_RESPONSE.setObject(authenticationService.getAuthenticatedUser());
			REST_RESPONSE.setSuccess(true);

			response.getWriter().print(objectMapper.writeValueAsString(REST_RESPONSE));
			response.getWriter().flush();

		} else {

			SavedRequest savedRequest = requestCache.getRequest(request, response);

			if (savedRequest == null) {
				clearAuthenticationAttributes(request);
				return;
			}
			String targetUrlParam = getTargetUrlParameter();

			if (isAlwaysUseDefaultTargetUrl()
					|| (targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
				requestCache.removeRequest(request, response);
				clearAuthenticationAttributes(request);
				return;
			}

			clearAuthenticationAttributes(request);

		}
	}

	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}
}
