package com.fortress.security;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class AuthenticatedUser extends User {

	private static final long serialVersionUID = -3381466747717666994L;

	private String username;

	private String sessionToken;

	private Set<String> authoritiesSet;

	private Set<String> getAuthoritiesSet() {
		if (this.authoritiesSet == null) {
			authoritiesSet = new TreeSet<String>();
			if (this.getAuthorities() != null) {
				for (GrantedAuthority ga : this.getAuthorities()) {
					authoritiesSet.add(ga.getAuthority());
				}
			}
		}
		return this.authoritiesSet;
	}

	public boolean hasPermission(String permission) {
		if (permission == null || permission.equals(""))
			return false;

		return this.getAuthoritiesSet().contains(permission);
	}

	public AuthenticatedUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);

		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

}
