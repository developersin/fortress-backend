package com.fortress.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fortress.bean.LoginRequest;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	    private String jsonUsername;
	    private String jsonPassword;
		private String CORS_ALLOWED_URLS = "http://localhost:4200";
		private List<String> corsAllowedUrls = new ArrayList<String>();
		private List<String> corsAllowedUrlsRegex = new ArrayList<String>();
		private static final String ORIGIN = "Origin";
		private static final String ALL = "*";
		
	    @Override
	    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response){
	    		    	
	    	corsAllowedUrlsRegex = Arrays.asList(CORS_ALLOWED_URLS.split(","));
			corsAllowedUrls = Arrays.asList(CORS_ALLOWED_URLS.split(","));
	    				
	    	if (request.getHeader(ORIGIN) != null && !request.getHeader(ORIGIN).equals("null")) {
				String requestOrigin = request.getHeader(ORIGIN);
				
				if (this.corsAllowedUrls.contains(ALL)) {
					response.setHeader("Access-Control-Allow-Origin", requestOrigin);
					
				}
				else {
					boolean matchFound = false;
					for (String allowedOrigin : corsAllowedUrls) {
						if (requestOrigin.equals(allowedOrigin)) {
								response.setHeader("Access-Control-Allow-Origin", requestOrigin);
								matchFound = true;
								break;
							}
					}
					
					if(matchFound == false)
					{
						for (String allowedOrigin : corsAllowedUrlsRegex) {
							if (requestOrigin.matches(allowedOrigin)) {
									response.setHeader("Access-Control-Allow-Origin", requestOrigin);
									matchFound = true;
									break;
								}
						}
					}
				}
			
				response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, origin, authorization, accept, client-security-token, error,response,success,x_csrf_token");
				response.setHeader("Access-Control-Expose-Headers", "x_csrf_token");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
			}
			if (request.getMethod().equals("OPTIONS")) {
				try {
					response.getWriter().print("OK");
					response.getWriter().flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} 
	    	
	        if ("application/json".equals(request.getHeader("Content-Type"))) {
	            try {
	                /*
	                 * HttpServletRequest can be read only once
	                 */
	                StringBuffer sb = new StringBuffer();
	                String line = null;

	                BufferedReader reader = request.getReader();
	                while ((line = reader.readLine()) != null){
	                    sb.append(line);
	                }

	                //json transformation
	                ObjectMapper mapper = new ObjectMapper();
	                LoginRequest loginRequest = mapper.readValue(sb.toString(), LoginRequest.class);

	                this.jsonUsername = loginRequest.getUsername();
	                this.jsonPassword = loginRequest.getPassword();
	                
	                System.out.println(this.jsonUsername);
	                
	                UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
	                		this.jsonUsername, this.jsonPassword);

	    			// Allow subclasses to set the "details" property
	    			setDetails(request, authRequest);

	    			return this.getAuthenticationManager().authenticate(authRequest);
	            
	        } catch (Exception e) {
	            System.out.println(e.getMessage());
	            throw new InternalAuthenticationServiceException("Failed to parse authentication request body");
	        }
	            
	        }
	        
	        return null;
	    }
	
}