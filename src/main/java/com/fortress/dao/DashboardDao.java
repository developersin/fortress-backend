package com.fortress.dao;

import org.springframework.stereotype.Repository;

import com.fortress.entity.Dashboard;

@Repository
public class DashboardDao extends AbstractDao<Dashboard>{

	public DashboardDao() {
		super(Dashboard.class);
	}

}
