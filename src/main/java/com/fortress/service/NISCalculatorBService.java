package com.fortress.service;

import com.fortress.bean.NISUserB;
import com.fortress.bean.NISUserBResult;

public interface NISCalculatorBService {

	public NISUserBResult calculateNISUserB( NISUserB nisUserB );
	
}
