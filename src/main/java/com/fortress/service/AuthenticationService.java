package com.fortress.service;

import com.fortress.security.AuthenticatedUser;

public interface AuthenticationService {

	public AuthenticatedUser getAuthenticatedUser();
	
}
