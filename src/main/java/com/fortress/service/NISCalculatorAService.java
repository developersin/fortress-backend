package com.fortress.service;

import com.fortress.bean.NISUserA;
import com.fortress.bean.NISUserAResult;

public interface NISCalculatorAService {

	public NISUserAResult calculateNISUserA( NISUserA nisUserA );

}
