package com.fortress.service;

import com.fortress.bean.NISUser;
import com.fortress.bean.NISUserResult;

public interface NISCalculatorService {

	public NISUserResult calculateNISUser( NISUser nisUser );
	
}
