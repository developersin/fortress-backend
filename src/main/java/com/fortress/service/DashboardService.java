package com.fortress.service;

import com.fortress.bean.UiDashboard;

public interface DashboardService {

	public UiDashboard save( UiDashboard bean );
	public UiDashboard get( Long id );
	public UiDashboard update(Long id, UiDashboard bean );
	
}
