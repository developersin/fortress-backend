package com.fortress.service.impl;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.AnnualCalculationBean;
import com.fortress.bean.NISUser;
import com.fortress.bean.NISUserResult;
import com.fortress.bean.Plan1CalculationResultBean;
import com.fortress.bean.Plan2CalculationBean;
import com.fortress.bean.Plan2CalculationResultBean;
import com.fortress.bean.RegularPensionContributions;
import com.fortress.bean.UiDashboard;
import com.fortress.service.DashboardService;
import com.fortress.service.NISCalculatorService;
import com.fortress.util.AppConstants;
import com.fortress.util.DateUtil;
import com.fortress.util.InnovaUtil;

@Service
public class NISCalculatorServiceImpl implements NISCalculatorService {

	@Resource
	private DashboardService dashboardService;
	
	@Override
	@Transactional
	public NISUserResult calculateNISUser(NISUser nisUser) {
		
		UiDashboard adminDetails = dashboardService.get(1l);

		NISUserResult result = new NISUserResult();

		result.setAmountOfInnova(10000f);

		int totalWorkingYears = 0;
		int yearsRemainingInRetirement = 0;
		int assumedWorkingAge = adminDetails.getAssumedWorkingAge();
		Float yearsFromStatementDateOfRetirement = 0.0f;
		Float monthlySalary = nisUser.getMonthlySalary();
		Float annualSalary = monthlySalary * 12;
		Float averageOfBestFiveYears = 0f;
		Float sumOfBestFiveYears = 0f;
		Float finalPension = 0f;
		Float mandatoryAgeCapPercentage = 0.0f;
		Float adjustedTotalCapValue = 0.0f;
		
		Float netInvestmentReturnAggressive = 4.90196078431373f;
		Float netInvestmentReturnOther = 2.94117647058825f;

		Date dateOfBirth = nisUser.getBirthdate();
		Date dateOfStatementOfRetirement = nisUser.getPensionStatementDate();
		Date todaysDate = Date.from(java.time.LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		
		Calendar dateOfBirthCalendar = Calendar.getInstance();
		dateOfBirthCalendar.setTime(dateOfBirth);

		Date dateOfRetirement = DateUtil.getDate((int) (dateOfBirthCalendar.get(Calendar.YEAR) + nisUser.getRetirementAge()),
				(int) dateOfBirthCalendar.get(Calendar.MONTH), (int) dateOfBirthCalendar.get(Calendar.DATE));
		
		Date dateOfWorkStarted = DateUtil.getDate((int) (dateOfBirthCalendar.get(Calendar.YEAR) + assumedWorkingAge), 
				(int) dateOfBirthCalendar.get(Calendar.MONTH), (int) dateOfBirthCalendar.get(Calendar.DATE));


		Float pensionForFirstTwentyYears = 0f;
		Float pensionForRemainingYears = 0f;

		ArrayList<AnnualCalculationBean> annualCalculationMap = new ArrayList<>();
		ArrayList<Plan2CalculationBean> plan2Calculations = new ArrayList<>();
		Plan1CalculationResultBean plan1CalculationResultBean = calculatePplan1(monthlySalary, annualSalary,
				nisUser.getEmployeeAVCContributions(), adminDetails);

		totalWorkingYears = DateUtil.getDiffYears(dateOfWorkStarted, dateOfRetirement);

		yearsRemainingInRetirement = (int) Math.floor(DateUtil.getDiffYears(todaysDate, dateOfRetirement));
		yearsFromStatementDateOfRetirement = (float) DateUtil.getDiffInFloat(dateOfStatementOfRetirement, dateOfRetirement);

		for (int i = 0; i <= 49; i++) {

			AnnualCalculationBean annualCalculationBean = new AnnualCalculationBean();
			Plan2CalculationBean plan2CalculationBean = new Plan2CalculationBean();

			annualCalculationBean.setYearsFromToday(i);
			annualCalculationBean.setAnnualCeilingMultiplier(
					annualMultiplier(AppConstants.ANNUAL_CEILING_INCREASE, annualCalculationBean.getYearsFromToday()));
			annualCalculationBean.setAnnualSlaryMultiplier(
					annualMultiplier(AppConstants.ANNUAL_SALARY_INCREASE, annualCalculationBean.getYearsFromToday()));
			annualCalculationBean
					.setAnnualCeilingMonthly(monthlySalary * annualCalculationBean.getAnnualCeilingMultiplier());
			annualCalculationBean
					.setAnnualCeilingAnnual(annualSalary * annualCalculationBean.getAnnualCeilingMultiplier());
			annualCalculationBean.setSalaryMonthly(monthlySalary * annualCalculationBean.getAnnualSlaryMultiplier());
			annualCalculationBean.setSalaryAnnual(annualSalary * annualCalculationBean.getAnnualSlaryMultiplier());

			// PPLAN 2 CALCULATIONS
			plan2CalculationBean.setYearsFromToday(i + 1);
			plan2CalculationBean.setSalaryMonthly(annualCalculationBean.getSalaryMonthly());
			plan2CalculationBean.setSalaryAnnual(annualCalculationBean.getSalaryAnnual());
			plan2CalculationBean.setSalaryInExcessOfInsured(
					(plan2CalculationBean.getSalaryAnnual() - annualCalculationBean.getAnnualCeilingAnnual()));
			plan2CalculationBean.setCeilingMonthly(annualCalculationBean.getAnnualCeilingMonthly());

			plan2CalculationBean.setMonthlyRegularContributions(
					monthlyRegularContributionCalculator(plan2CalculationBean.getSalaryMonthly(),
							plan2CalculationBean.getCeilingMonthly(), plan1CalculationResultBean));
			plan2CalculationBean.setMonthlyAVCContributions(
					monthlyAVCContributionCalculator(plan2CalculationBean.getSalaryMonthly(),
							plan2CalculationBean.getCeilingMonthly(), plan1CalculationResultBean));

			plan2CalculationBean.setTotalMonthlyContributions(plan2CalculationBean.getMonthlyRegularContributions()
					+ plan2CalculationBean.getMonthlyAVCContributions());

			if (nisUser.getInvestmentStyle() == AppConstants.InvestmentStyle.AGGRESSIVE) {
				plan2CalculationBean.setFvMonthlyContributions( (float) fv( (netInvestmentReturnAggressive / 12)/100.0f , 12,
						-plan2CalculationBean.getTotalMonthlyContributions(), 0, 0));

				plan2CalculationBean.setCompounded( (float) (plan2CalculationBean.getFvMonthlyContributions() * 
						Math.pow( (float) (1 + netInvestmentReturnAggressive/100.0000), yearsFromStatementDateOfRetirement - plan2CalculationBean.getYearsFromToday())));
			} else {
				plan2CalculationBean.setFvMonthlyContributions( (float) fv((netInvestmentReturnOther / 12)/100.0f, 12,
						-plan2CalculationBean.getTotalMonthlyContributions(), 0, 0));
				
				plan2CalculationBean.setCompounded( (float) (plan2CalculationBean.getFvMonthlyContributions() * 
						Math.pow( (1+netInvestmentReturnOther/100.0000), (yearsFromStatementDateOfRetirement - plan2CalculationBean.getYearsFromToday()))));
			}
			
			if( i > 0 ) {
				plan2CalculationBean.setCumulativeMonthlyContributions( plan2Calculations.get( i - 1  ).getCumulativeMonthlyContributions() + plan2CalculationBean.getCompounded());
			} else {
				plan2CalculationBean.setCumulativeMonthlyContributions( plan2CalculationBean.getCompounded() );
			}

			annualCalculationMap.add(annualCalculationBean);
			plan2Calculations.add(plan2CalculationBean);
		}

		Plan2CalculationResultBean plan2CalculationResultBean = new Plan2CalculationResultBean(); 

		Float statemenetFutureValue = 0.0f;
		
		if(nisUser.getInvestmentStyle() == AppConstants.InvestmentStyle.AGGRESSIVE) {
			statemenetFutureValue = (float) ((nisUser.getRecentPensionStatementBalance() )*(  Math.pow( (1 + netInvestmentReturnAggressive/100.000f), yearsFromStatementDateOfRetirement )));
		} else {
			statemenetFutureValue = (float) ((nisUser.getRecentPensionStatementBalance() )*(  Math.pow( (1 + netInvestmentReturnOther/100.00f), yearsFromStatementDateOfRetirement )));
		}
		
		plan2CalculationResultBean.setStatemenetFutureValue(statemenetFutureValue);
		plan2CalculationResultBean.setMonthlyFutureContribution( plan2Calculations.get( (int) Math.floor( yearsFromStatementDateOfRetirement) - 1 ).getCumulativeMonthlyContributions() );

		for (int j = yearsRemainingInRetirement, i = 0; i < 5; i++, j--) {
			sumOfBestFiveYears += annualCalculationMap.get(j).getAnnualCeilingAnnual();
		}

		averageOfBestFiveYears = (sumOfBestFiveYears / 5);

		pensionForFirstTwentyYears = averageOfBestFiveYears * 20.00f * AppConstants.FIRST_20_YEARS;
		pensionForRemainingYears = averageOfBestFiveYears * (totalWorkingYears - 20) * AppConstants.REMAINING_YEARS;

		if( nisUser.getRetirementAge() <= 70 ) {
			mandatoryAgeCapPercentage = (100.00f + (adminDetails.getMandatoryRetirementAge() - nisUser.getRetirementAge()) * (-0.5f) * 12.00f) / 100.00f; 
		}
		
//		finalPension = averageOfBestFiveYears * AppConstants.CAP_PERCENTAGE;
		adjustedTotalCapValue = averageOfBestFiveYears * AppConstants.CAP_PERCENTAGE;
		
		if( annualCalculationMap.get(yearsRemainingInRetirement).getAnnualCeilingAnnual() * AppConstants.CAP_PERCENTAGE < 0.6 ) {
			finalPension = pensionForFirstTwentyYears + pensionForRemainingYears;
		} else {
			finalPension = adjustedTotalCapValue * mandatoryAgeCapPercentage;
		}

		result.setAnnualNISPension(finalPension);
		result.setTotalPensionPlanValue( plan2CalculationResultBean.getStatemenetFutureValue() + plan2CalculationResultBean.getMonthlyFutureContribution() );
		result.setTaxFreeAmountOfRetirement( result.getTotalPensionPlanValue() * 0.25f );
		result.setAmountOfInnova( result.getTotalPensionPlanValue() - result.getTaxFreeAmountOfRetirement() );
		
		result.setEstimatedInnovaPensionPayoutFirstYear( InnovaUtil.innovaFinder(nisUser.getRetirementAge()) * result.getAmountOfInnova() );
		result.setTotalEstimatedPensionIncome( result.getAnnualNISPension() + result.getEstimatedInnovaPensionPayoutFirstYear() );
		
		BigDecimal incomeAsPercentageOfSalary = new BigDecimal(Float.toString(((result.getTotalEstimatedPensionIncome()/annualCalculationMap.get(yearsRemainingInRetirement).getSalaryAnnual()) * 100.00f)));
		incomeAsPercentageOfSalary = incomeAsPercentageOfSalary.setScale(2, BigDecimal.ROUND_HALF_UP);
		
		result.setPensionIncomeAsPercentageOfSalary( (float) Math.ceil( incomeAsPercentageOfSalary.floatValue() ));
		result.setLumpsum( (float) Math.ceil( result.getTotalPensionPlanValue() * 0.25f ) );
		result.setTransferOfInnova( (float) Math.ceil( result.getTotalPensionPlanValue() * 0.75f ) );
		
		return result;
	}
	
	private Float annualMultiplier(Float increaseRate, int years) {
		return (float) Math.pow((1f + increaseRate), years);
	}

	private Float fv(Float r, int nper, Float pmt, int pv, int type) {
		float retval = 0; 
        if (r == 0) { 
            retval = -1*(pv+(nper*pmt)); 
        } 
        else { 
            float r1 = r + 1; 
            retval =  (float) ((1- (float) Math.pow(r1, nper)) * (type == 1 ? r1 : 1) * pmt ) / r 
                      - (float) pv * (float) Math.pow(r1, nper); 
        } 
		
		return retval;
	}

	private Float monthlyRegularContributionCalculator(Float monthlySalary, Float ceilingMonthlySalary,
			Plan1CalculationResultBean plan1CalculationResult) {

		Float monthlyRegularContribution = 0.0f;

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyRegularContribution += plan1CalculationResult.getEmployeeRegularContributionBelowNISCeiling()
					* ceilingMonthlySalary;
		} else {
			monthlyRegularContribution += plan1CalculationResult.getEmployeeRegularContributionBelowNISCeiling()
					* monthlySalary;
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyRegularContribution += plan1CalculationResult.getEmployerRegularContributionBelowNISCeiling()
					* ceilingMonthlySalary;
		} else {
			monthlyRegularContribution += plan1CalculationResult.getEmployerRegularContributionBelowNISCeiling()
					* monthlySalary;
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyRegularContribution += plan1CalculationResult.getEmployeeRegularContributionAboveNISCeiling()
					* (monthlySalary - ceilingMonthlySalary);
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyRegularContribution += plan1CalculationResult.getEmployerRegularContributionAboveNISCeiling()
					* (monthlySalary - ceilingMonthlySalary);
		}

		return monthlyRegularContribution;
	}

	private Float monthlyAVCContributionCalculator(Float monthlySalary, Float ceilingMonthlySalary,
			Plan1CalculationResultBean plan1CalculationResult) {

		Float monthlyAVCContribution = 0.0f;

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyAVCContribution += plan1CalculationResult.getEmployeeVoluntaryContributionBelowNISCeiling()
					* ceilingMonthlySalary;
		} else {
			monthlyAVCContribution += plan1CalculationResult.getEmployeeVoluntaryContributionBelowNISCeiling()
					* monthlySalary;
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyAVCContribution += plan1CalculationResult.getEmployerVoluntaryContributionBelowNISCeiling()
					* ceilingMonthlySalary;
		} else {
			monthlyAVCContribution += plan1CalculationResult.getEmployerVoluntaryContributionBelowNISCeiling()
					* monthlySalary;
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyAVCContribution += plan1CalculationResult.getEmployeeVoluntaryContributionAboveNISCeiling()
					* (monthlySalary - ceilingMonthlySalary);
		}

		if (monthlySalary > ceilingMonthlySalary) {
			monthlyAVCContribution += plan1CalculationResult.getEmployerVoluntaryContributionAboveNISCeiling()
					* (monthlySalary - ceilingMonthlySalary);
		}

		return monthlyAVCContribution;
	}

	private Plan1CalculationResultBean calculatePplan1(Float monthlySalary, Float annualSalary,
			Float employeeAVCContributions, UiDashboard adminDetails) {

		Plan1CalculationResultBean plan1CalculationResultBean = new Plan1CalculationResultBean();

		// GET THESE VALUES FROM DATABASE LATER
		Float employerRegularContributionBelowCeiling = adminDetails.getRegularContributionBelowCeiling() / 100.00f;
		Float employerRegularContributionAboveCeiling = adminDetails.getRegularContributionAboveCeiling() / 100.00f;

		Float employerAVCContributionBelowCeiling = adminDetails.getAvcContributionBelowCeiling() / 100.00f;
		Float employerAVCContributionAboveCeiling = adminDetails.getAvcContributionAboveCeiling() / 100.00f;
		Float nisCeiling = adminDetails.getNisCeiling();

		RegularPensionContributions regularPensionContributions = new RegularPensionContributions();

		// GET FROM DATABASE THESE TWO VALUES
		regularPensionContributions.setEmployeeBelowNISCeiling(0.02F);
		regularPensionContributions.setEmployeeAboveNISCeiling(0.05f);
		regularPensionContributions.setEmployerAVCContributionBelowNISCeiling(employerRegularContributionBelowCeiling);
		regularPensionContributions.setEmployerAVCContributionAboveNISCeiling(employerRegularContributionAboveCeiling);

		regularPensionContributions.setEmployeeBelowNISCeilingCalculation(
				monthlySalary > nisCeiling ? regularPensionContributions.getEmployeeBelowNISCeiling() * nisCeiling
						: regularPensionContributions.getEmployeeBelowNISCeiling() * monthlySalary);

		regularPensionContributions.setEmployerBelowNISCeilingCalculation(monthlySalary > nisCeiling
				? regularPensionContributions.getEmployerAVCContributionBelowNISCeiling() * nisCeiling
				: regularPensionContributions.getEmployerAVCContributionBelowNISCeiling() * monthlySalary);

		regularPensionContributions.setEmployeeAboveNISCeilingCalculation(monthlySalary > nisCeiling
				? regularPensionContributions.getEmployeeAboveNISCeiling() * (monthlySalary - nisCeiling)
				: 0);

		regularPensionContributions.setEmployerAboveNISCeilingCalculation(monthlySalary > nisCeiling
				? regularPensionContributions.getEmployerAVCContributionAboveNISCeiling() * (monthlySalary - nisCeiling)
				: 0);

		regularPensionContributions
				.setTotalRegularPensionContributions(regularPensionContributions.getEmployeeAboveNISCeilingCalculation()
						+ regularPensionContributions.getEmployeeBelowNISCeilingCalculation()
						+ regularPensionContributions.getEmployerAboveNISCeilingCalculation()
						+ regularPensionContributions.getEmployerBelowNISCeilingCalculation());

		Float employerVoluntaryContributionBelowCeiling = employeeAVCContributions == 0 ? 0
				: employerAVCContributionBelowCeiling;
		Float employerVoluntaryContributionAboveCeiling = employeeAVCContributions == 0 ? 0
				: employerAVCContributionAboveCeiling;

		Float employeeVoluntaryContributionBelowCeilingCalculation = monthlySalary > nisCeiling
				? nisCeiling * employeeAVCContributions
				: monthlySalary * employeeAVCContributions;
		Float employeeVoluntaryContributionAboveCeilingCalculation = monthlySalary > nisCeiling
				? employeeAVCContributions * (monthlySalary - nisCeiling)
				: 0;

		Float employerVoluntaryContributionBelowCeilingCalculation = monthlySalary > nisCeiling
				? nisCeiling * employerVoluntaryContributionBelowCeiling
				: monthlySalary * employerVoluntaryContributionBelowCeiling;
		Float employerVoluntaryContributionAboveCeilingCalculation = monthlySalary > nisCeiling
				? employerVoluntaryContributionAboveCeiling * (monthlySalary - nisCeiling)
				: 0;

		Float totalContribution = employeeVoluntaryContributionBelowCeilingCalculation
				+ employeeVoluntaryContributionAboveCeilingCalculation
				+ employerVoluntaryContributionBelowCeilingCalculation
				+ employerVoluntaryContributionAboveCeilingCalculation;

		plan1CalculationResultBean.setEmployeeRegularContributionBelowNISCeiling(
				regularPensionContributions.getEmployeeBelowNISCeiling());
		plan1CalculationResultBean.setEmployeeRegularContributionAboveNISCeiling(
				regularPensionContributions.getEmployeeAboveNISCeiling());

		plan1CalculationResultBean.setEmployerRegularContributionBelowNISCeiling(
				regularPensionContributions.getEmployerAVCContributionBelowNISCeiling());
		plan1CalculationResultBean.setEmployerRegularContributionAboveNISCeiling(
				regularPensionContributions.getEmployerAVCContributionAboveNISCeiling());

		plan1CalculationResultBean.setEmployeeVoluntaryContributionBelowNISCeiling(employeeAVCContributions);
		plan1CalculationResultBean.setEmployeeVoluntaryContributionAboveNISCeiling(employeeAVCContributions);

		plan1CalculationResultBean
				.setEmployerVoluntaryContributionBelowNISCeiling(employerVoluntaryContributionBelowCeiling);
		plan1CalculationResultBean
				.setEmployerVoluntaryContributionAboveNISCeiling(employerVoluntaryContributionAboveCeiling);

		return plan1CalculationResultBean;
	}

}
