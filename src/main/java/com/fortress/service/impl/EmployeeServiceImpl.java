package com.fortress.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.UiEmployee;
import com.fortress.converter.EmployeeConverter;
import com.fortress.dao.EmployeeDao;
import com.fortress.entity.Employee;
import com.fortress.service.DashboardService;
import com.fortress.service.EmployeeService;

@Service
@Transactional( readOnly = true )
public class EmployeeServiceImpl implements EmployeeService{

	@Resource
	private EmployeeDao employeeDao;
	
	@Resource
	private EmployeeConverter employeeConverter;
	
	@Resource
	private DashboardService dashboardService;
	
	@Transactional( readOnly = false)
	@Override
	public void addNewEmployee(UiEmployee employee) throws Exception{
		
		Employee entity = employeeConverter.getEntityFromBean( employee );
		
		employeeDao.create(entity);
		
	}

	@Override
	@Transactional
	public List<UiEmployee> getAllEmployees() throws Exception{
		
		List<Employee> entities = new ArrayList<>();
		
		entities = employeeDao.findAll();
		
		List<UiEmployee> beans = new ArrayList<>();
		
		beans = employeeConverter.getBeansFromEntities(entities);
		
		return beans;
	}

	@Override
	@Transactional
	public Employee getAuthenticUserByUsername(String username) throws Exception{
		
      return employeeDao.findByUsername(username);
	}

	@Override
	@Transactional
	public UiEmployee get(Long id) throws Exception {

		UiEmployee bean = new UiEmployee();
		Employee entity = employeeDao.findOne(id);
		bean = employeeConverter.getBeanFromEntity( entity );
		
		if( entity.getDashboardId() != null ) {
		 bean.setDashboard( dashboardService.get(entity.getDashboardId()) );
		}
		 
		return bean;
		
	}
	
}
