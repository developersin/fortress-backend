package com.fortress.service.impl;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.NISUserB;
import com.fortress.bean.NISUserBResult;
import com.fortress.service.NISCalculatorBService;
import com.fortress.util.DateUtil;

@Service
public class NISCalculatorBServiceImpl implements NISCalculatorBService {

	@Override
	@Transactional
	public NISUserBResult calculateNISUserB(NISUserB nisUserB) {

		Float yearsRemainingInRetirement = 0.0f;
		Float monthsRemainingInRetirement = 0f;
		Date dateOfBirth = nisUserB.getBirthdate();
		Date todaysDate = Date.from(java.time.LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar dateOfBirthCalendar = Calendar.getInstance();
		dateOfBirthCalendar.setTime(dateOfBirth);
		
		Date dateOfRetirement = DateUtil.getDate((int) (dateOfBirthCalendar.get(Calendar.YEAR) + nisUserB.getRetirementAge()),
				(int) dateOfBirthCalendar.get(Calendar.MONTH), (int) dateOfBirthCalendar.get(Calendar.DATE));
		
		NISUserBResult result = new NISUserBResult();

		yearsRemainingInRetirement = (float) DateUtil.getDiffInExcelFrac(todaysDate, dateOfRetirement);
		monthsRemainingInRetirement = yearsRemainingInRetirement * 12.00f; 
		
		System.out.println(yearsRemainingInRetirement);
		System.out.println(monthsRemainingInRetirement);
		System.out.println(nisUserB.getInterestRate());
		
		result.setTotalEstimatedAccumulatedPension(
				 (int) ( ( (float) Math.pow( (1 + nisUserB.getInterestRate()) , yearsRemainingInRetirement) * nisUserB.getCurrentSavingsInPension() ) +
				( nisUserB.getMonthlySavings() * (  ( (float) Math.pow( (1 + ( nisUserB.getInterestRate() / 12.00f )), monthsRemainingInRetirement ) - 1 ) / ( nisUserB.getInterestRate() / 12.00f )  ) ) )				
				);
		
		return result;
	}

}
