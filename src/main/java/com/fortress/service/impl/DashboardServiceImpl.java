package com.fortress.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.UiDashboard;
import com.fortress.converter.DashboardConverter;
import com.fortress.dao.DashboardDao;
import com.fortress.entity.Dashboard;
import com.fortress.service.DashboardService;

@Service
public class DashboardServiceImpl implements DashboardService{

	@Resource
	private DashboardDao dashboardDao;
	
	@Resource
	private DashboardConverter dashboardConverter;
	
	@Override
	@Transactional
	public UiDashboard save(UiDashboard bean) {

		Dashboard entity = dashboardConverter.getEntityFromBean(bean);
		dashboardDao.create(entity);
		
		return get(entity.getId());
	}

	@Override
	@Transactional
	public UiDashboard get(Long id) {
		
		return dashboardConverter.getBeanFromEntity( dashboardDao.findOne( id ) );
	}

	@Override
	@Transactional
	public UiDashboard update(Long id, UiDashboard bean) {

		Dashboard entity = dashboardDao.findOne( id );
		dashboardConverter.populateEntityWithBean(entity, bean);
		
		dashboardDao.update(entity);
		
		return get(entity.getId());
	}

}
