package com.fortress.service.impl;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortress.bean.NISUserA;
import com.fortress.bean.NISUserAResult;
import com.fortress.service.NISCalculatorAService;
import com.fortress.util.DateUtil;
import com.fortress.util.ExcelUtils;

@Service
public class NISCalculatorAServiceImpl implements NISCalculatorAService{

	@Override
	@Transactional
	public NISUserAResult calculateNISUserA(NISUserA nisUserA) {

		NISUserAResult result = new NISUserAResult();
				
		Float yearsRemainingInRetirement = 0.0f;
		Float monthsRemainingInRetirement = 0f;
		Date dateOfBirth = nisUserA.getBirthdate();
		Date todaysDate = Date.from(java.time.LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Calendar dateOfBirthCalendar = Calendar.getInstance();
		dateOfBirthCalendar.setTime(dateOfBirth);
		
		Date dateOfRetirement = DateUtil.getDate((int) (dateOfBirthCalendar.get(Calendar.YEAR) + nisUserA.getRetirementAge()),
				(int) dateOfBirthCalendar.get(Calendar.MONTH), (int) dateOfBirthCalendar.get(Calendar.DATE));
		
		yearsRemainingInRetirement = (float) DateUtil.getDiffInExcelFrac(todaysDate, dateOfRetirement);
		monthsRemainingInRetirement = yearsRemainingInRetirement * 12.00f; 
		
		if( ExcelUtils.pmt( (-nisUserA.getInterestRate()/12.00f) ,  monthsRemainingInRetirement, 
				(nisUserA.getExpectedAccumulationByRetirement() - (( Math.pow( ( 1 + nisUserA.getInterestRate()), yearsRemainingInRetirement ) * nisUserA.getCurrentSavingsInPension())) ), 0, 1) > 0 ) {						
			
		} else {
			
		result.setMonthlyInvestmentRequiredToAchieveTarget(	
				(float) Math.ceil( -ExcelUtils.pmt( (-nisUserA.getInterestRate()/12.00f) ,  monthsRemainingInRetirement, 
					(nisUserA.getExpectedAccumulationByRetirement() - (( Math.pow( ( 1 + nisUserA.getInterestRate()), yearsRemainingInRetirement ) * nisUserA.getCurrentSavingsInPension())) ), 0, 1) ) );
		}
		
		return result;
	}

}
