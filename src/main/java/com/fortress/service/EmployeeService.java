package com.fortress.service;

import java.util.List;

import com.fortress.bean.UiEmployee;
import com.fortress.entity.Employee;

public interface EmployeeService {

	public void addNewEmployee(UiEmployee employee) throws Exception;
	
	public List<UiEmployee> getAllEmployees() throws Exception;
	
	public Employee getAuthenticUserByUsername( String username ) throws Exception;
	
	public UiEmployee get( Long id ) throws Exception;
	
}
