package com.fortress.config;

import java.util.Arrays;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.fortress.security.CustomUsernamePasswordAuthenticationFilter;
import com.fortress.security.MySavedRequestAwareAuthenticationSuccessHandler;
import com.fortress.security.RestAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity( prePostEnabled = true )
@ComponentScan("com.fortress")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
 
    @Autowired
    private MySavedRequestAwareAuthenticationSuccessHandler
      authenticationSuccessHandler;
    
    @Resource(name = "userAuthDetailService")
    private UserDetailsService userDetailsService;
    
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth)
//      throws Exception {
//  
//        auth.inMemoryAuthentication()
//          .withUser("temporary").password("temporary").roles("ADMIN")
//          .and()
//          .withUser("user").password("userPass").roles("USER");
//    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception { 
        http
        .csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint(restAuthenticationEntryPoint)
        .and().addFilterBefore( authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
        .formLogin().loginPage("/login").permitAll()
        .successHandler(authenticationSuccessHandler)
        .failureHandler(new SimpleUrlAuthenticationFailureHandler())
        .and()
        .logout();
        http.cors();
        http.authorizeRequests().antMatchers("/niscalculatorb").permitAll();
        http.authorizeRequests().antMatchers("/niscalculatora").permitAll();
        http.authorizeRequests().antMatchers("/niscalculator").permitAll();
        http.authorizeRequests().antMatchers("/dashboard").hasAuthority("DASHBOARD_READ_WRITE");
//        http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll();
    }
    
    
    
    @Bean
    public CustomUsernamePasswordAuthenticationFilter authenticationFilter() throws Exception{
    	
    	CustomUsernamePasswordAuthenticationFilter authFilter = new CustomUsernamePasswordAuthenticationFilter();
    	
    	authFilter.setRequiresAuthenticationRequestMatcher( new AntPathRequestMatcher("/login", "POST") );
    	authFilter.setAuthenticationManager(authenticationManagerBean());
    	authFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
    	authFilter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler());
    	authFilter.setUsernameParameter("username");
    	authFilter.setPasswordParameter("password");
    	
    	return authFilter;
    	
    }
 
    @Bean
    public MySavedRequestAwareAuthenticationSuccessHandler mySuccessHandler(){
        return new MySavedRequestAwareAuthenticationSuccessHandler();
    }
    @Bean
    public SimpleUrlAuthenticationFailureHandler myFailureHandler(){
        return new SimpleUrlAuthenticationFailureHandler();
    }

    @Override
    protected void configure( AuthenticationManagerBuilder authenticationManagerBuilder ) throws Exception
    {
    	authenticationManagerBuilder.userDetailsService(userDetailsService);
    	
    }
    
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(  Arrays.asList("HEAD",
                "GET", "POST", "PUT", "DELETE", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}