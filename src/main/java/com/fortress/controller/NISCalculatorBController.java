package com.fortress.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fortress.bean.BaseResponsePojo;
import com.fortress.bean.NISUserB;
import com.fortress.bean.NISUserBResult;
import com.fortress.service.NISCalculatorBService;

@RestController
public class NISCalculatorBController {

	@Resource
	private NISCalculatorBService nisCalculatorBService;

	@PostMapping(path = { "niscalculatorb" })
	public BaseResponsePojo<Long, NISUserBResult> getAllEmployees(@RequestBody NISUserB nisUserB) {

		BaseResponsePojo<Long, NISUserBResult> response = new BaseResponsePojo<Long, NISUserBResult>();

		try {
			response.setObject(nisCalculatorBService.calculateNISUserB(nisUserB));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

}
