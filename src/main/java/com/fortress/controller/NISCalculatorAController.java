package com.fortress.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fortress.bean.BaseResponsePojo;
import com.fortress.bean.NISUserA;
import com.fortress.bean.NISUserAResult;
import com.fortress.service.NISCalculatorAService;

@RestController
public class NISCalculatorAController {

	@Resource
	private NISCalculatorAService nisCalculatorAService;

	@PostMapping(path = { "niscalculatora" })
	public BaseResponsePojo<Long, NISUserAResult> getAllEmployees(@RequestBody NISUserA nisUserA) {

		BaseResponsePojo<Long, NISUserAResult> response = new BaseResponsePojo<Long, NISUserAResult>();

		try {
			response.setObject(nisCalculatorAService.calculateNISUserA(nisUserA));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}
	
}
