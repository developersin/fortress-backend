package com.fortress.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fortress.bean.BaseResponsePojo;
import com.fortress.bean.UiDashboard;
import com.fortress.service.DashboardService;

@RestController
public class DashboardController {

	@Resource
	private DashboardService dashboardService;

	@PostMapping(path = { "dashboard" })
	public BaseResponsePojo<Long, UiDashboard> save(@RequestBody UiDashboard bean) {

		BaseResponsePojo<Long, UiDashboard> response = new BaseResponsePojo<Long, UiDashboard>();

		try {
			response.setObject(dashboardService.save(bean));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	@PutMapping(path = { "dashboard/{id}" })
	public BaseResponsePojo<Long, UiDashboard> update(@PathVariable Long id, @RequestBody UiDashboard bean) {

		BaseResponsePojo<Long, UiDashboard> response = new BaseResponsePojo<Long, UiDashboard>();

		try {
			response.setObject(dashboardService.update(id, bean));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	@GetMapping(path = { "dashboard/{id}" })
	public BaseResponsePojo<Long, UiDashboard> get(@PathVariable Long id) {

		BaseResponsePojo<Long, UiDashboard> response = new BaseResponsePojo<Long, UiDashboard>();

		try {
			response.setObject(dashboardService.get(id));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

}
