package com.fortress.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fortress.bean.BaseResponsePojo;
import com.fortress.bean.NISUser;
import com.fortress.bean.NISUserB;
import com.fortress.bean.NISUserBResult;
import com.fortress.bean.NISUserResult;
import com.fortress.service.NISCalculatorBService;
import com.fortress.service.NISCalculatorService;

@RestController
public class NISCalculatorController {

	@Resource
	private NISCalculatorService nisCalculatorService;
		
	@PostMapping(path = { "niscalculator" })
	public BaseResponsePojo<Long, NISUserResult> getAllEmployees(@RequestBody NISUser nisUser) {				

		BaseResponsePojo<Long, NISUserResult> response = new BaseResponsePojo<Long, NISUserResult>();
		
		try {
			response.setObject(nisCalculatorService.calculateNISUser(nisUser));
			response.setSuccess(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;

	}

}
