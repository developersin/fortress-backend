package com.fortress.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Employee {

	private static final long serialVersionUID = 5063397042741005211L;

	private Long id;

	private String name;
	private String password;
	private Integer salary;
	private Long dashboardId;
	private Dashboard dashboard;
	private Set<BasePermission> basePermissions = new HashSet<BasePermission>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getDashboardId() {
		return dashboardId;
	}

	public void setDashboardId(Long dashboardId) {
		this.dashboardId = dashboardId;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "dashboardId", nullable = false, insertable = false, updatable = false)
	public Dashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}
	
	@ManyToMany(cascade=CascadeType.ALL)
//    @JoinTable(name="BasePermission", joinColumns={@JoinColumn(referencedColumnName="ID", updatable=false)}
//                                        , inverseJoinColumns={@JoinColumn(referencedColumnName="ID")}) 
	public Set<BasePermission> getBasePermissions() {
		return basePermissions;
	}

	public void setBasePermissions(Set<BasePermission> basePermissions) {
		this.basePermissions = basePermissions;
	}

}
