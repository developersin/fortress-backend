package com.fortress.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dashboard {

	private Long id;

	private Float regularContributionBelowCeiling;
	private Float regularContributionAboveCeiling;
	private Float avcContributionBelowCeiling;
	private Float avcContributionAboveCeiling;
	private Float nisCeiling;
	private int mandatoryRetirementAge;
	private int assumedWorkingAge;
	private Float aggressive;
	private Float conservative;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getRegularContributionBelowCeiling() {
		return regularContributionBelowCeiling;
	}

	public void setRegularContributionBelowCeiling(Float regularContributionBelowCeiling) {
		this.regularContributionBelowCeiling = regularContributionBelowCeiling;
	}

	public Float getRegularContributionAboveCeiling() {
		return regularContributionAboveCeiling;
	}

	public void setRegularContributionAboveCeiling(Float regularContributionAboveCeiling) {
		this.regularContributionAboveCeiling = regularContributionAboveCeiling;
	}

	public Float getAvcContributionBelowCeiling() {
		return avcContributionBelowCeiling;
	}

	public void setAvcContributionBelowCeiling(Float avcContributionBelowCeiling) {
		this.avcContributionBelowCeiling = avcContributionBelowCeiling;
	}

	public Float getAvcContributionAboveCeiling() {
		return avcContributionAboveCeiling;
	}

	public void setAvcContributionAboveCeiling(Float avcContributionAboveCeiling) {
		this.avcContributionAboveCeiling = avcContributionAboveCeiling;
	}

	public Float getNisCeiling() {
		return nisCeiling;
	}

	public void setNisCeiling(Float nisCeiling) {
		this.nisCeiling = nisCeiling;
	}

	public int getMandatoryRetirementAge() {
		return mandatoryRetirementAge;
	}

	public void setMandatoryRetirementAge(int mandatoryRetirementAge) {
		this.mandatoryRetirementAge = mandatoryRetirementAge;
	}

	public int getAssumedWorkingAge() {
		return assumedWorkingAge;
	}

	public void setAssumedWorkingAge(int assumedWorkingAge) {
		this.assumedWorkingAge = assumedWorkingAge;
	}

	public Float getAggressive() {
		return aggressive;
	}

	public void setAggressive(Float aggressive) {
		this.aggressive = aggressive;
	}

	public Float getConservative() {
		return conservative;
	}

	public void setConservative(Float conservative) {
		this.conservative = conservative;
	}

}
