package com.fortress.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BasePermission {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2719026928394874774L;
	
	private Long id;

	private String name;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		boolean equal = false;
		if((obj != null) && (obj instanceof BasePermission))
		{
			BasePermission object = (BasePermission) obj;
			
			if(this.getId() == object.getId()
					|| this.getName().equalsIgnoreCase(object.getName()))
					{
						equal = true;
					}
			
		}
		
		return equal;
	}
	
}
